package com.fr.isika.table8table8;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;


@Controller
public class Table8Controller {

	public Personne personne;
	public String name;
	
	@RequestMapping("/")
	public String accueil(Personne personne) {
	return "accueil";
	}

	@GetMapping(path = "/welcome")
	public String welcome(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		model.addAttribute("name", name);
		return "welcome";
		
	}
}