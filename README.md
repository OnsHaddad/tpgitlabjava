# tpGitlabJava
---
# Dockerfile:

... le Dockerfile contient: ([en utilisant ce lien ](https://stackoverflow.com/questions/27767264/how-to-dockerize-maven-project-and-how-many-ways-to-accomplish-it) )
```
 #
... # Build stage
...#
...FROM maven:3.6.0-jdk-11-slim AS build
...COPY src /home/app/src
...COPY pom.xml /home/app
...RUN mvn -f /home/app/pom.xml clean package
---
```
## pour builder en local : docker build -t demo .

